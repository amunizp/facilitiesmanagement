# Estate Management

## Why?

Neighbours of an estate are busy. But there is always a few neighbours who want to take control and foster community. When that happens there is the possibility of two options: the hero who does everthing or the reduction of barriers so that the rest can provide their bitty help.

### Things that have been tried

**Technical side of things:**
Smart sheets + Google App Sheet. They have templates that you follow which do the trick especially in task management and reporting. They are no code platforms that enable a good default view and arrangements. But their pricing model means that if you want to start adding users it becomes too much. They are based on spreadsheets which means more things to track and difficulty in interrelationship. Most of these require an app on phone and or a login to online account.

**Social Side of things**
Reporting via email, phone calls, instant messaging private conversations, ... this does not aim to replace that as people are busy but they do want to be heard and know that they are taken seriously. But there always will be a neighbor that will be able to distill that information and if you want an update you can find it yourself or you can ask and it should be readily available.

**What we want to move away from**
Committee member a, moves on and unknowingly takes away loads of experience. Committee member b does not know history and starts to reinvent the wheel. Contractors loose track of who to speak to.
Outstanding issues are forgotten.
Each person has their own spreadsheet or a shared spreadsheet is downloaded and updated locally but not updated.
Even if done correctly, spreadsheet then needs to link to doc, to a particular heading that will be updated and dates and other information will need to be added manually.
Letter writing is done manually and separately when "hello ${flat number } of ${court} is more inviting.


**Proof of concept**

- Vanilla JS front and back with mongoDB test: [Rock the code Project](https://rock-the-code-proyecto10-front.vercel.app/Home)

### Other open source projects

- https://github.com/microrealestate/microrealestate this one is mainly about a leaseholder charging fees. IMHO service charges should be separate and routinely set as a standing order.
- https://github.com/orpms/orpms seems to be archived but has some interesting features: Gather all information of your properties and occupants in one place and Template letters.
- https://github.com/pHAlkaline/phkondo/wiki seems to have a section about maintenance have not tested. Does include charges.
- https://www.openmaint.org/en/resources/screenshots interesting features such as calendar view, dashboards, GIS, maintance manual, ... looks huge!
- [CiviCRM.org](https://civicrm.org/) might be a good option if we want to go the route of identifing users in a secure way, sending emails, reports, (...). An addon might be useful. Intuitively making the location a constituent  might be the way to go? we are not tracking people more like "things" or "locations"
    - Costume made Addon explored on this branch: https://gitlab.com/amunizp/facilitiesmanagement/-/tree/civicrm-extension?ref_type=heads
    - [civiPledge](https://docs.civicrm.org/user/en/latest/pledges/what-is-civipledge/) is an interesting thing for participants pleging their time: eg. "I will be fire rep for this block for a year" or "I will be part fo the committee for the next 2 years"
    - [CiviEverydayTasks](https://docs.civicrm.org/user/en/latest/case-management/everyday-tasks/) could be the task viewer?
    - [CiviCase](https://docs.civicrm.org/user/en/latest/case-management/what-is-civicase/) could be the issue tracker?
    - Extras: civiPetition to get quorum on a proposal, civiSurvey to collect fire inspecitons forms?





### Non open source projects

- https://www.fixflo.com/
- [Facilities Template google appsheet](https://www.appsheet.com/templates/Inspect-points-throughout-various-properties?appGuid=d8389191-0217-42c0-bd50-406993b660f6)
- [smart sheet](https://www.smartsheet.com/) using an existing template
- [Monday .com](https://monday.com/lang/es) finding and fine tooning an existing template.
- [civica](https://gitlab.com/amunizp/facilitiesmanagement/-/tree/civicrm-extension?ref_type=heads) seems to provide many different solutions and is a [UK Gov approved supplier?](https://www.applytosupply.digitalmarketplace.service.gov.uk/g-cloud/services/886549736864009) 
- [Dwellant](https://www.dwellant.com/) in particular the CSite.

## Example Situations

Example:

- "I told you about the issue in my window a month ago, where is it at?" Have a look at the issue tracker and see the different updates o come and talk to me and I will find the issue and give you an update.
- "It has been a while since the window cleaner? when are they due to come?" Check our calendar for our routine visits.
- "I have seen that a decorator is coming by to paint the garage doors, is he coming to paint mine?" check on the issue tracker or linked calendar appointment to see if that task was assigned to them. OR click on your garage on the map and see what jobs and issues have been assigned to it.
- "What is my wall made out of? " , "Do we have asbestos? " , "How is the joinery made?", "how do I replace a lightbulb", ... here is the link to our website that will have that information available.


## Journeys

[source file for editting PNG bellow](Presentation/Journeys.svg)

### UX draft Presentation

[Sozi Presentation download ](Presentation/Proposal.sozi.html)

[Sozi Presentation preview?](https://amunizp.gitlab.io/facilitiesmanagement/Presentation/Proposal.sozi.html)

### Participant

![flow diagram of Participant](Presentation/ParticipantJourneys.png)

### Elevated Participant

![flow diagram of Elevated Participant](Presentation/ElevatedParticipantJourneys.png)


### Operator Participant

![flow diagram of Elevated Participant](Presentation/OperatorParticipantJourneys.png)

